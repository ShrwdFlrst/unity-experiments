using UnityEngine;
using System.Collections;

public class FreeflightCamera : MonoBehaviour
{
    public float speedNormal = 10.0f;
    public float speedFast = 50.0f;

    public float mouseSensitivityX = 5.0f;
    public float mouseSensitivityY = 5.0f;

    float rotY = 0.0f;

    bool showText = true;
    Rect textArea = new Rect(0, 0, Screen.width, Screen.height);
    string debugText = "";

    GUIStyle myStyle;

    GameObject cube;
    AudioSource audio;

    void Start()
    {
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;

        myStyle = new GUIStyle();
        myStyle.fontSize = 40;
        myStyle.normal.textColor = Color.white;
        cube = GameObject.Find("Cube");
        audio = new AudioSource();
        Cursor.visible = false;
        
    }

    void Update()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, fwd, out hit, 1000))
        {
            if (hit.collider.name != debugText)
            {
                debugText = hit.collider.name;

                if (hit.collider.name == cube.name)
                {
                    if (audio != null)
                    {
                        audio.Stop();
                    }
                    audio = cube.GetComponent<AudioSource>();
                    audio.Play();
                }
            }

            if (hit.collider.name == cube.name)
            {
                cube.transform.Rotate(1, 0, 0);
                // Application.LoadLevel("scene02");
            }
        }

        // rotation        
        //if (Input.GetMouseButton(1))
        //{
            float rotX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * mouseSensitivityX;
            rotY += Input.GetAxis("Mouse Y") * mouseSensitivityY;
            rotY = Mathf.Clamp(rotY, -89.5f, 89.5f);
            transform.localEulerAngles = new Vector3(-rotY, rotX, 0.0f);
        //}

        if (Input.GetKey(KeyCode.U))
        {
            gameObject.transform.localPosition = new Vector3(0.0f, 3500.0f, 0.0f);
        }

        float forward = Input.GetAxis("Vertical");
        float strafe = Input.GetAxis("Horizontal");

        // move forwards/backwards
        if (forward != 0.0f)
        {
            float speed = Input.GetKey(KeyCode.LeftShift) ? speedFast : speedNormal;
            Vector3 trans = new Vector3(0.0f, 0.0f, forward * speed * Time.deltaTime);
            gameObject.transform.localPosition += gameObject.transform.localRotation * trans;
        }

        // strafe left/right
        if (strafe != 0.0f)
        {
            float speed = Input.GetKey(KeyCode.LeftShift) ? speedFast : speedNormal;
            Vector3 trans = new Vector3(strafe * speed * Time.deltaTime, 0.0f, 0.0f);
            gameObject.transform.localPosition += gameObject.transform.localRotation * trans;
        }
    }

    void OnGUI()
    {

        if (showText)
        {
            GUI.Label(textArea, debugText, myStyle);
        }

    }
}
