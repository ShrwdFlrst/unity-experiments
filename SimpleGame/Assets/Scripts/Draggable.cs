﻿using UnityEngine;
using System.Collections;

public class Draggable : MonoBehaviour
{

    Transform grabbed;
    float grabDistance = 1000.0f;
    int grabLayerMask;
    Vector3 grabOffset; //delta between transform transform position and hit point
    GameObject holding;
    GameObject player;

    void Start()
    {
        player = GameObject.Find("FPSController");
    }

    void Update()
    {
        UpdateToggleDrag();
    }

    // Toggles drag with mouse click
    void UpdateToggleDrag()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Grab();
        }
        else
        {
            if (holding)
            {
                Drag();
            }
        }
    }


    void Grab()
    {
        if (holding)
        {
            grabbed = null;
            holding = null;
            //grabbed.gameObject.layer = grabLayerMask;
            // holding.GetComponent<Rigidbody>().useGravity = true;
        }
        else
        {
            RaycastHit hit;
            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            
            if (Physics.Raycast(transform.position, fwd, out hit, 1000))
            {                
                if (hit.transform.gameObject.layer == 8)
                {
                    holding = GameObject.Find(hit.collider.name);
                    print(holding.layer);
                    print(hit.collider.name);                    

                    
                    //holding.transform.Translate(player.transform.position + player.transform.forward + Vector3.up * 0.14f);
                    
                    //// holding.GetComponent<Rigidbody>().useGravity = false;
                    //grabbed = hit.transform;
                    //if (grabbed.parent)
                    //{
                    //    grabbed = grabbed.parent.transform;
                    //}
                    ////set the object to ignore raycasts
                    //grabLayerMask = grabbed.gameObject.layer;
                    //grabbed.gameObject.layer = 2;
                    
                    ////now immediately do another raycast to calculate the offset
                    //if (Physics.Raycast(transform.position, fwd, out hit, 1000))
                    //{
                    //    grabOffset = grabbed.position - hit.point;
                    //}
                    //else
                    //{
                    //    //important - clear the gab if there is nothing
                    //    //behind the object to drag against
                    //    grabbed = null;
                    //}
                }

            }
        }
    }
    void Drag()
    {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        holding.transform.position = new Vector3(player.transform.position.x, 1.4f, player.transform.position.z) + (new Vector3(fwd.x, 0, fwd.z)) * 3.0f;
        //RaycastHit hit;
        //
        
        //if (Physics.Raycast(transform.position, fwd, out hit, 1000))
        //{
        //    grabbed.position = hit.point + grabOffset;
        //}

    }
}
