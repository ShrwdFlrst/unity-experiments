﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.TutorialAssets.Scripts
{
    class GuiCrossHair : MonoBehaviour 
    {
        public Texture2D crosshairTexture;
        public float crosshairScale = 1;
        void OnGUI()
        {
            //if not paused
            if (Time.timeScale != 0)
            {
                if (crosshairTexture != null)
                    GUI.DrawTexture(new Rect((Screen.width - crosshairTexture.width * crosshairScale) / 2, 
                        (Screen.height - crosshairTexture.height * crosshairScale) / 2, crosshairTexture.width * crosshairScale, crosshairTexture.height * crosshairScale), crosshairTexture);
                else
                    Debug.Log("No crosshair texture set in the Inspector");
            }
        }

        void Update()
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
